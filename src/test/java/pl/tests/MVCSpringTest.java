package pl.tests;

import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.chris.controller.BasicController;
import pl.chris.model.Persons;
import pl.chris.repository.PersonRepository;

import static org.mockito.internal.verification.VerificationModeFactory.atLeastOnce;

public class MVCSpringTest {

	@Test
	public void shouldReturnHomePage() throws Exception {
		PersonRepository repoMock = Mockito.mock(PersonRepository.class);
		BasicController controller = new BasicController(repoMock);
		MockMvc mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
		mockMvc.perform(MockMvcRequestBuilders.get("/")).andExpect(MockMvcResultMatchers.view().name("index"));
	}

	@Test
	public void shouldDisplayRegisterForm() throws Exception {
		PersonRepository repoMock = Mockito.mock(PersonRepository.class);
		BasicController controller = new BasicController(repoMock);
		MockMvc mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
		mockMvc.perform(MockMvcRequestBuilders.get("/register")).andExpect(MockMvcResultMatchers.view().name("registerForm"));
	}

	@Test
	public void shouldProcessForm() throws Exception {

		PersonRepository mockRepository = Mockito.mock(PersonRepository.class);

		Persons unsaved = new Persons("Jan", "Kowalski");
		Persons saved = new Persons(234, "Jan", "Kowalski");
		BasicController controller = new BasicController(mockRepository);

		Mockito.when(mockRepository.save(unsaved)).thenReturn(saved);

		MockMvc mockMvc = MockMvcBuilders.standaloneSetup(controller).build();

		mockMvc.perform(MockMvcRequestBuilders.post("/save").param("firstName", "Jan").param("lastName", "Kowalski")).andExpect(
				MockMvcResultMatchers.redirectedUrl("/listAll.html"));

		Mockito.verify(mockRepository, atLeastOnce()).save(unsaved);
	}

	@Test
	public void shouldFailValidationWithNoData() throws Exception {

		PersonRepository mockRepository = Mockito.mock(PersonRepository.class);
		BasicController controller = new BasicController(mockRepository);
		MockMvc mockMvc = MockMvcBuilders.standaloneSetup(controller).build();

		mockMvc.perform(MockMvcRequestBuilders.post("/save")).andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.view().name("registerForm"))
			//	.andExpect(model().errorCount(2))
				.andExpect(MockMvcResultMatchers.model().attributeErrorCount("person", 2))
				.andExpect(MockMvcResultMatchers.model().attributeHasFieldErrors("person", "firstName", "lastName"));
	}

	@Test
	public void shouldSavePerson() throws Exception {
		
		PersonRepository mockRepository = Mockito.mock(PersonRepository.class);
		BasicController controller = new BasicController(mockRepository);
		MockMvc mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
		
		Persons p = new Persons("Jan", "Dlugosz");
		
		mockMvc.perform(MockMvcRequestBuilders.post("/save").flashAttr("person", p)).andExpect(MockMvcResultMatchers.model().hasNoErrors()).andExpect(MockMvcResultMatchers.status().is3xxRedirection());
		
		// mockMvc.perform(post("/save").param("firstName","Jan").param("lastName", "Kowalski"))
		// .andExpect(model().hasNoErrors()).andExpect(redirectedUrl("/listAll.html"));
		// .andExpect(status().is3xxRedirection());
	}
}
