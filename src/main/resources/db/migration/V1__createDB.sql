CREATE TABLE `Persons` (
  `id_person` INT(11)  AUTO_INCREMENT NOT NULL ,
  `firstName` VARCHAR(30),
  `lastName`  VARCHAR(30),
  PRIMARY KEY (`id_person`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = UTF8;
/**
EXAMPLE USAGE
mvn flyway:clean -Dflyway.schema=PROD -Dflyway.url=jdbc:mysql://localhost:3306/testowa -Dflyway.user=root -Dflyway.password=admin1234
mvn flyway:migrate -Dflyway.schema=PROD -Dflyway.url=jdbc:mysql://localhost:3306/testowa -Dflyway.user=root -Dflyway.password=admin1234


mvn flyway:clean -Dflyway.schema=testowa -Dflyway.url=jdbc:mysql://localhost:3306/testowa -Dflyway.user=root -Dflyway.password=admin1234
mvn flyway:migrate -Dflyway.schema=testowa -Dflyway.url=jdbc:mysql://localhost:3306/testowa -Dflyway.user=root -Dflyway.password=admin1234
 */