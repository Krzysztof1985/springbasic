var app = angular.module('testApp', []);


app.controller("testCtrl", function ($scope) {

    $scope.possibleChoice = ["Alabama", "Georgia", "Washington"];


    $scope.defaultSelection = $scope.possibleChoice[0];

    $scope.changedItem = function (itemName) {
        console.log('ITEM NAME ' + itemName);
        $scope.preview = itemName;
    }
});