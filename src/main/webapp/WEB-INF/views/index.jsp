<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en" ng-app="testApp">
<head>
    <link href="resources/css/bootstrap.css" rel="stylesheet"/>
    <link href="resources/css/bootstrap-theme.css" rel="stylesheet"/>
    <script type="text/javascript" src="resources/js/angular.min.js"></script>
    <script type="text/javascript" src="resources/js/testCtrl.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Welcome in SPIRNG 4 MVC</title>
</head>
<body ng-controller="testCtrl">
<h1>
    <s:message code="app.welcome" text="Witaj"/>
</h1>
<a href="test.html">TEST FOR SPRING 4</a>
<br/>
<a href="listAll.html">List all</a>
<br/>
<a href="register.html">Register</a>

<div class="form-group">
    <select ng-model="defaultSelection" ng-options="o as o for o in possibleChoice" ng-init="defaultSelection"
            ng-change="changedItem(defaultSelection)"></select>

<pre>
    {{preview}}
</pre>
</div>
</body>
</html>