<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<style>
.error {
	color: #ff0000;
}

.errorblock {
	color: #000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
	width: 120px;
}
</style>
<body>
	<c:url var="savePersonURL" value="save.html" />
	<div>
		<form:form modelAttribute="person" method="POST"
			action="${savePersonURL}">
			<form:errors path="*" cssClass="errorblock" element="div" />
			<br />
			<br />
			<form:label path="firstName">Person Name:</form:label>
			<form:input path="firstName" size="30" />
			<br />
			<br />
			<form:label path="lastName">Person Surname:</form:label>
			<form:input path="lastName" size="30" />
			<br />
			<br />
			<input type="submit" value="Submit" />
		</form:form>
	</div>
</body>
</html>