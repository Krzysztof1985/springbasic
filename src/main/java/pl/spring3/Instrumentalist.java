package pl.spring3;

public class Instrumentalist implements Performer {

	private String song;
	private Instrument instrument;

	@Override
	public void perform() throws PerformanceException {
		System.out.print("Playing " + song + " : ");

		instrument.play();
	}

	public void setSong(String song) {
		this.song = song;
	}

	public String getSong() {
		return song;
	}

	public Instrument getInstrument() {
		return instrument;
	}

	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}
}
