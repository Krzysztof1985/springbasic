package pl.spring3;

public class Piano implements Instrument {

	@Override
	public void play() {
		System.out.println("Piano plays : plim plim plim");
	}

}
