package pl.spring3.starter;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import pl.spring3.PerformanceException;
import pl.spring3.Performer;

public class SpringLauncher {

	public static void main(String[] args) throws PerformanceException {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");

		Performer performer = (Performer) ctx.getBean("kenny");
		performer.perform();
	}

}
