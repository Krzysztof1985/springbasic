package pl.spring3;

public class Juggler implements Performer {

	public Juggler(int beanBags) {
		this.beanBags = beanBags;
	}
	private int beanBags = 3;

	public Juggler() {
	}

	public int getBeanBags() {
		return beanBags;
	}

	public void setBeanBags(int beanBags) {
		this.beanBags = beanBags;
	}

	@Override
	public void perform() throws PerformanceException {
		System.out.println("JUggling " + beanBags + " beanbags");
	}

}
