package pl.spring3;

public interface Performer {

	public void perform() throws PerformanceException;
}
