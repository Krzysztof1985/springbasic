package pl.spring3;

public class Saxophone implements Instrument {

	@Override
	public void play() {
		System.out.println("Saxophone played : TOOT TOOT TOOT");
	}
}
