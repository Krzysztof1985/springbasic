package pl.spring3;

public interface Instrument {

	public void play();
}
