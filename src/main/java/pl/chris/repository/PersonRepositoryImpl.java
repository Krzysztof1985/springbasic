package pl.chris.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.ldap.userdetails.Person;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.chris.model.Persons;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.List;

@Service
@EnableTransactionManagement
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, isolation = Isolation.SERIALIZABLE)
public class PersonRepositoryImpl implements PersonRepository {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public List<Persons> selectAll() {
		// List<Persons> persons = (List<Persons>)
		// getCurrentSession().createCriteria(Persons.class).list();

		List<Persons> list = (List<Persons>) getCurrentSession().createQuery("from Persons").list();
		return list;
	}

	@Override
	@Transactional(readOnly = false)
	public Persons save(Persons p) {
		getCurrentSession().save(p);
		return p;
	}

	@Override
	public Persons findById(int id) {
		return (Persons) getCurrentSession().get(Persons.class, id);
	}
}
