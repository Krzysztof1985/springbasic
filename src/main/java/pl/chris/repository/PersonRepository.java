package pl.chris.repository;

import java.util.List;

import org.springframework.security.ldap.userdetails.Person;
import pl.chris.model.Persons;

public interface PersonRepository {

	public List<Persons> selectAll();
	
	public Persons save(Persons p);

	public Persons findById(int id);
}
