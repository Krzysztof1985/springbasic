package pl.chris.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.*;

@Entity
@Table(name = "Persons")
@XmlRootElement(name = "Person")
@XmlAccessorType(XmlAccessType.FIELD)
public class Persons implements Serializable {

	public Persons() {
	}

	public Persons(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public Persons(int id_preson, String firstName, String lastName) {
		this.id = id_preson;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	private static final long serialVersionUID = -6154910864212149021L;

	@Id
	@GeneratedValue
	@XmlAttribute(name = "id")
	@Column(name = "id_person")
	private int id;

	@NotNull(message = "First name cannot be empty")
	@Size(min = 3, max = 30, message = "First name cannot be shorter than 1 and longer than 30 chars")
	@Column(name = "firstName")
	@XmlAttribute(name="firstName")
	private String firstName;

	@NotNull(message = "Last name cannot be empty")
	@Size(min = 3, max = 30, message = "Last name cannot be shorter than 1 and longer than 30 chars")
	@Column(name = "lastName")
	@XmlAttribute(name="lastName")
	private String lastName;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "Persons [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + id;
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Persons other = (Persons) obj;
		if (firstName == null) {
			if (other.firstName != null) return false;
		}
		else if (!firstName.equals(other.firstName)) return false;
		if (id != other.id) return false;
		if (lastName == null) {
			if (other.lastName != null) return false;
		}
		else if (!lastName.equals(other.lastName)) return false;
		return true;
	}
}
