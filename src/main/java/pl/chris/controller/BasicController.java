package pl.chris.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.chris.model.Persons;
import pl.chris.repository.PersonRepository;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class BasicController {

	public BasicController() {
	}

	@Autowired
	PersonRepository repository;

	public BasicController(PersonRepository repository) {
		this.repository = repository;
	}

	private static final Logger logger = Logger.getLogger(BasicController.class);

	@RequestMapping(value = "/", method = GET)
	public String homePage() {
		return "index";
	}

	@RequestMapping(value = "/test", method = GET)
	public String testPage() {
		return "testPage";
	}

	@RequestMapping(value = "/listAll", method = GET)
	public String listAll(ModelMap model) {
		List<Persons> allUsers = repository.selectAll();
		model.put("lista", allUsers);
		return "list";
	}

	@RequestMapping(value = "/login", method = GET)
	public String login() {
		return "login";
	}

	@RequestMapping(value = "/login", method = POST)
	public String loginPost() {
		return "login";
	}

	@RequestMapping(value = "/register", method = GET)
	public String register(@ModelAttribute("person") Persons person, ModelMap model) {
		model.addAttribute("person", new Persons());
		System.out.println("Model -->" + model.toString());
		return "registerForm";
	}

	@RequestMapping(value = "/save", method = POST)
	public ModelAndView savePerson(@ModelAttribute("person") @Valid Persons person, Errors errors) {
		if (errors.hasErrors()) {
			System.out.println("SA BLEDY ###");
			logger.error("ERRORES ");
			return new ModelAndView("registerForm");
		}

		repository.save(person);
		return new ModelAndView("redirect:/listAll.html");
	}
}
