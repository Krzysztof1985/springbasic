package pl.chris.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.chris.model.Persons;
import pl.chris.repository.PersonRepository;

import java.util.List;

@RestController
@RequestMapping("/rest")
public class RestControllerHandler {

    @Autowired
    PersonRepository repository;

    @RequestMapping(value = "/findAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Persons>> findAll() {
        return new ResponseEntity<List<Persons>>(repository.selectAll(), HttpStatus.OK);
    }


    @RequestMapping(value = "/find/{personId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Persons> findByIdRest(@PathVariable("personId") int personId) {
        return new ResponseEntity<Persons>(repository.findById(personId), HttpStatus.OK);
    }

    @RequestMapping(value = "/id/{personId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Persons> findById(@PathVariable("personId") int personId) {
        System.out.println("Given person id " + personId);
        return new ResponseEntity<Persons>(repository.findById(personId), HttpStatus.OK);
    }

    @RequestMapping(value = "/findAllXML", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public List<Persons> findAllXML() {
        return repository.selectAll();
    }

}
