package pl.chris.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class AdviseControllerHandler {
    private static final String GENERAL_ERROR = "duplicate";

    @ExceptionHandler(Exception.class)
    public String duplicateSpittleHandler(Exception ex) {
        System.out.println(ex.getMessage());
        return GENERAL_ERROR;
    }
}
