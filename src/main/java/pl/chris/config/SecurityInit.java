package pl.chris.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
import org.springframework.stereotype.Component;

@Component
public class SecurityInit extends AbstractSecurityWebApplicationInitializer {

}
