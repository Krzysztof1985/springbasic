package pl.spring4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import pl.spring3.*;

@Configuration
public class SpringConfig {

	@Bean
	public Performer juggler(int beanBags) {
		Juggler juggler = new Juggler(beanBags);
		return juggler;
	}

	@Bean
	public Performer poeticJuggler(int beanBags, Poem poem) {
		PoeticJuggler poetic = new PoeticJuggler(beanBags, poem);
		return poetic;
	}

	@Bean
	public Performer instrumentalist() {
		Instrumentalist musician = new Instrumentalist();
		musician.setSong("Turlaj jurlaj");
		musician.setInstrument(saxophone());
		return musician;
	}

	@Bean
	@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.INTERFACES)
	public Instrument piano() {
		return new Piano();
	}

	@Autowired
	@Primary
	@Bean
	public Instrument saxophone() {
		return new Saxophone();
	}

}
