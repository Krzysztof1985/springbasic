package pl.spring4;

import pl.spring3.PerformanceException;
import pl.spring3.Performer;
import pl.spring3.Sonnet29;

public class Spring4Launcher {

	public static void main(String[] args) throws PerformanceException {
		SpringConfig config = new SpringConfig();

		Performer musician = config.instrumentalist();
		musician.perform();
		Sonnet29 sonnet29 = new Sonnet29();
		Performer performer = config.poeticJuggler(3, sonnet29);
		performer.perform();
	}

}
